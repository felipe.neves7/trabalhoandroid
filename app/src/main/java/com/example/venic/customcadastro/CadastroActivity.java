package com.example.venic.customcadastro;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

public class CadastroActivity extends AppCompatActivity {
    private EditText edtNome;
    private EditText edtIdade;
    private EditText edtNacionalidade;
    private EditText edtTelefone;
    private EditText edtEndereco;
    private Spinner edtSexo;
    private Spinner edtEstadoCivil;
    private EditText edtCor;
    private Spinner edtEscolaridade;
    private EditText edtProfissao;


    public static final int CADASTRO_USUARIO = 100;
    public static final int RESULT_CADASTRO_USUARIO_NOVO = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        edtNome = (EditText) findViewById(R.id.cpNome);
        edtIdade= (EditText) findViewById(R.id.cpIdade);
        edtNacionalidade= (EditText) findViewById(R.id.cpNacionalidade);
        edtTelefone= (EditText) findViewById(R.id.cpTelefone);
        edtEndereco= (EditText) findViewById(R.id.cpEndereco);
        edtSexo= (Spinner) findViewById(R.id.cpSexo);
        edtEstadoCivil= (Spinner) findViewById(R.id.cpEstadoCivil);
        edtCor= (EditText) findViewById(R.id.cpCor);
        edtEscolaridade= (Spinner) findViewById(R.id.cpEscolaridade);
        edtProfissao= (EditText) findViewById(R.id.cpProfissao);

    }


    private boolean isCampoVazio(String valor){
        return (TextUtils.isEmpty(valor) || valor.trim().isEmpty() || valor == "");
    }
    

    public void cadastrar(View view) {

        String name = edtNome.getText().toString();
        String age = edtIdade.getText().toString();
        String nationality = edtNacionalidade.getText().toString();
        String phone =edtTelefone.getText().toString();
        String adress = edtEndereco.getText().toString();
        String sex = edtSexo.getSelectedItem().toString();
        String stateCivil = edtEstadoCivil.getSelectedItem().toString();
        String color = edtCor.getText().toString();
        String schooling = edtEscolaridade.getSelectedItem().toString();
        String profession = edtProfissao.getText().toString();

        boolean res = false;

        if(res = isCampoVazio(name)){
            edtNome.requestFocus();
        }
        else if(res = isCampoVazio(age)){
            edtIdade.requestFocus();
        }
        else if(res = isCampoVazio(adress)){
            edtNacionalidade.requestFocus();
        }
        else if(res = isCampoVazio(phone)){
            edtTelefone.requestFocus();
        }
        else if(res = isCampoVazio(adress)){
            edtEndereco.requestFocus();
        }
        else if(res = isCampoVazio(sex)){
            edtSexo.requestFocus();
        }
        else if(res = isCampoVazio(stateCivil)){
            edtEstadoCivil.requestFocus();
        }
        else if(res = isCampoVazio(color)){
            edtCor.requestFocus();
        }
        else if(res = isCampoVazio(schooling)){
            edtEscolaridade.requestFocus();
        }
        else if(res = isCampoVazio(profession)){
            edtProfissao.requestFocus();
        }


        if(res){
            AlertDialog.Builder dlg = new AlertDialog.Builder(this);
            dlg.setTitle("Aviso");
            dlg.setMessage("Campos Inválidos ou em Branco");
            dlg.setNeutralButton("OK",null);
            dlg.show();
        }
        else{

            age = "Idade: " + edtIdade.getText().toString();
            nationality = "Nacionalidade: " +edtNacionalidade.getText().toString();
            phone = "Telefone: " +edtTelefone.getText().toString();
            adress = "Endereço: " +edtEndereco.getText().toString();
            sex = "Sexo: " +edtSexo.getSelectedItem().toString();
            stateCivil = "Estado Civil: " +edtEstadoCivil.getSelectedItem().toString();
            color = "Cor: " + edtCor.getText().toString();
            schooling = "Escolaridade: " +edtEscolaridade.getSelectedItem().toString();
            profession = "Profissão: " +edtProfissao.getText().toString();


            Pessoa pessoa;
            pessoa = new Pessoa(name,age,
                    phone,adress,nationality,sex,stateCivil,color,schooling,profession);

            Intent dadosRetorno = new Intent();
            dadosRetorno.putExtra("pessoa", pessoa);
            setResult(RESULT_CADASTRO_USUARIO_NOVO, dadosRetorno);
            finish();
        }


    }
}
