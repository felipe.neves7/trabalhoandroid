package com.example.venic.customcadastro;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.MyHolder> {
    Context context;
    private final List<Pessoa> mList;
    public ListAdapter(Context context, final  List<Pessoa> mList) {
        this.context = context;
        this.mList = mList;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view  = LayoutInflater.from(this.context).inflate(R.layout.item_pessoa,viewGroup, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder viewHolder, int i) {
        // Seta os valores do item na lista
        final int pos = i;

        viewHolder.nome.setText(mList.get(i).getNome());
        viewHolder.idade.setText(mList.get(i).getIdade());
        viewHolder.telefone.setText(mList.get(i).getTelefone());
        viewHolder.endereco.setText(mList.get(i).getEndereco());
        viewHolder.nacionalidade.setText(mList.get(i).getNacionalidade());
        viewHolder.sexo.setText(mList.get(i).getSexo());
        viewHolder.estadoCivil.setText(mList.get(i).getEstadoCivil());
        viewHolder.corPreferida.setText(mList.get(i).getCorPreferida());
        viewHolder.escolaridade.setText(mList.get(i).getEscolaridade());
        viewHolder.profissao.setText(mList.get(i).getProfissao());

    }

    @Override
    public int getItemCount() {
        return this.mList.size();
    }



    public class MyHolder extends RecyclerView.ViewHolder {

        public TextView nome;
        public TextView idade;
        public TextView telefone;
        public TextView endereco;
        public TextView nacionalidade;
        public TextView sexo;
        public TextView estadoCivil;
        public TextView corPreferida;
        public TextView escolaridade;
        public TextView profissao;



        public MyHolder (View itemView) {
            super(itemView);
            nome = (TextView) itemView.findViewById(R.id.tv_nome);
            idade = (TextView) itemView.findViewById(R.id.tv_idade);
            telefone = (TextView) itemView.findViewById(R.id.tv_telefone);
            endereco = (TextView) itemView.findViewById(R.id.tv_endereco);
            nacionalidade = (TextView) itemView.findViewById(R.id.tv_nacionalidade);
            sexo = (TextView) itemView.findViewById(R.id.tv_sexo);
            estadoCivil = (TextView) itemView.findViewById(R.id.tv_estado);
            corPreferida = (TextView) itemView.findViewById(R.id.tv_cor);
            escolaridade = (TextView) itemView.findViewById(R.id.tv_escolaridade);
            profissao = (TextView) itemView.findViewById(R.id.tv_profissao);
        }
    }
}
