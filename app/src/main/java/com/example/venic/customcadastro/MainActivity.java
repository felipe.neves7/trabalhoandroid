package com.example.venic.customcadastro;

import android.content.Intent;
        import android.os.Bundle;
        import android.support.annotation.Nullable;
        import android.support.design.widget.FloatingActionButton;
        import android.support.design.widget.Snackbar;
        import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
        import android.support.v7.widget.RecyclerView;
        import android.support.v7.widget.Toolbar;
        import android.view.View;

        import java.util.ArrayList;
        import java.util.List;

public class MainActivity extends AppCompatActivity {
    FloatingActionButton fab;
    RecyclerView rv_pessoa;
    RecyclerView.Adapter adapter;
    List<Pessoa> mList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mList = new ArrayList<>();

        rv_pessoa = (RecyclerView) findViewById(R.id.rv_pessoa);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 1);

        rv_pessoa.setLayoutManager(layoutManager);
        adapter = new ListAdapter(getBaseContext(), mList);
        rv_pessoa.setAdapter(adapter);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(getBaseContext(), CadastroActivity.class), CadastroActivity.CADASTRO_USUARIO);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == CadastroActivity.CADASTRO_USUARIO) {
            if (resultCode == CadastroActivity.RESULT_CADASTRO_USUARIO_NOVO) {
                Pessoa p = (Pessoa)data.getSerializableExtra("pessoa");
                mList.add(p);
                adapter.notifyDataSetChanged();
                adapter.notifyItemInserted(mList.size());

                Snackbar.make(fab , "Item Inserido", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }

    }
}
