package com.example.venic.customcadastro;

import java.io.Serializable;

public class Pessoa implements Serializable {
    private String nome;
    private String idade;
    private String telefone;
    private String endereco;
    private String nacionalidade;
    private String sexo;
    private String estadoCivil;
    private String cor;
    private String escolaridade;
    private String profissao;


    public Pessoa(String nome, String endereco) {
        this.nome = nome;
        this.endereco = endereco;
    }

    public  Pessoa(String nome, String idade, String telefone, String endereco, String nacionalidade, String sexo,
                   String estadoCivil, String cor, String escolaridade, String profissao){
        super();
        this.nome = nome;
        this.idade = idade;
        this.telefone = telefone;
        this.endereco = endereco;
        this.nacionalidade = nacionalidade;
        this.sexo = sexo;
        this.estadoCivil = estadoCivil;
        this.cor = cor;
        this.escolaridade = escolaridade;
        this.profissao = profissao;


    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getIdade() {
        return idade;
    }

    public void setIdade(String idade) {
        this.idade = idade;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getNacionalidade() {
        return nacionalidade;
    }

    public void setNacionalidade(String nacionalidade) {
        this.nacionalidade = nacionalidade;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getCorPreferida() {
        return cor;
    }

    public void setCorPreferida(String cor) {
        this.cor = cor;
    }

    public String getEscolaridade() {
        return escolaridade;
    }

    public void setEscolaridade(String escolaridade) {
        this.escolaridade = escolaridade;
    }

    public String getProfissao() {
        return profissao;
    }

    public void setProfissao(String profissao) {
        this.profissao = profissao;
    }

    @Override
    public String toString() {
        return nome +
                idade +
                telefone +
                endereco +
                nacionalidade +
                sexo +
                estadoCivil +
                cor +
                escolaridade +
                profissao ;
    }
}
